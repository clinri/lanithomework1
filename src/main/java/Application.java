import animals.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik vasya = new Kotik("Вася", "мэу", 3, 3000);
        Kotik masha = new Kotik("Машка");
        masha.setVoice("мяяяу");
        masha.setSatiety(4);
        masha.setWeight(2500);
        for (String s : vasya.liveAnotherDay()) {
            System.out.println(s);
        }
        System.out.println("Котик по имени " + masha.getName() + " весит " + masha.getWeight() + "  грамм.");
        compareVoice(vasya,masha);
        System.out.println(Kotik.getCount());
    }

    static boolean compareVoice(Kotik kotik1, Kotik kotik2) {
        System.out.println("Котик " + kotik1.getName() + " говорит \"" + kotik1.getVoice() + "\"");
        System.out.println("Котик " + kotik2.getName() + " говорит \"" + kotik2.getVoice() + "\"");
        boolean equalsVoice = kotik1.getVoice().equals(kotik2.getVoice());
        System.out.println("Результат сравнения голосов котиков: " + (equalsVoice ? "голоса одинаковые!" : "голоса разные!"));
        return kotik1.getVoice().equals(kotik2.getVoice());
    }
}
