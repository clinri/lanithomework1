package employee;

import animals.Animal;
import animals.Voice;
import food.Food;
import food.WrongFoodException;

public class Worker {

    public void feed(Animal animal, Food food) {
        // покормить
        System.out.println("Работник зоопарка дал " + food + " животному " + animal);
        try {
            animal.eat(food);
        } catch (WrongFoodException e){
            System.out.println(e.getStackTrace());
        }
    }

    public void getVoice(Voice animal) {
        //попросить подать голос
        Voice voiceable = animal;
        System.out.println("Работник зоопарка попросил " + animal + " издать звук...");
        System.out.println(voiceable.getVoice());
    }
}
