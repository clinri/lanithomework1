package model;

import animals.Animal;
import java.util.HashMap;

public class Aviary<T extends Animal> {
    private Size size;
    private HashMap<String,T> aviaryMap = new HashMap<>();

    public Aviary(Size size) {
        this.size = size;
    }

    public void addAnimal(T animal) {
        if (animal.getSize() == size) {
            aviaryMap.put(animal.getName(), animal);
        } else {
            throw  new WrongSizeException("You try add " + animal.getSize() + " in Aviary " + size);
        }
    }

    public T getAnimal(String name){
        return aviaryMap.get(name);
    }

    public boolean removeAnimal(String name){
        if (aviaryMap.containsKey(name)){
            aviaryMap.remove(name);
            return true;
        } else {
            return false;
        }
    }
}
