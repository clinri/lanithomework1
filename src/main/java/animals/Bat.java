package animals;

import model.Size;

public class Bat extends Carnivorous implements Fly, Voice {
    public Bat(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }

    @Override
    public void fly() {
        System.out.println("Я лечу безшумно");
    }

    @Override
    public String getVoice() {
        return "П-с-с-с";
    }
}
