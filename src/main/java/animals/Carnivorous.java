package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;

public abstract class Carnivorous extends Animal {
    public Carnivorous(String name) {
        super(name);
        satiety += 10;
    }

    @Override
    public void eat(Food meat) throws WrongFoodException {
        System.out.println("Пробую есть");

        if (meat instanceof Meat) {
            satiety += meat.getEnergy() * 2;
            System.out.println("ням-ням, мясо я люблю, моя сытость увеличилась на " + meat.getEnergy() * 2);
        } else {
            //System.out.println("Фу-у-у я такое не ем.");
            throw new WrongFoodException("Вы дали недопустимый тип пищи животному");
        }
    }

    @Override
    public String toString() {
        return super.toString() + " \"" + name + "\" (хищник)";
    }
}