package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

public abstract class Herbivore extends Animal {
    public Herbivore(String name) {
        super(name);
    }

    @Override
    public void eat(Food grass) throws WrongFoodException {
        System.out.println("Пробую есть");
        if (grass instanceof Grass) {
            satiety += grass.getEnergy();
            System.out.println("ням-ням, травку я люблю, моя сытость увеличилась на" + grass.getEnergy());
        } else {
            //System.out.println("Фу-у-у я такое не ем.");
            throw new WrongFoodException("Вы дали недопустимый тип пищи животному");
        }
    }

    @Override
    public String toString() {
        return super.toString() + " \"" + name + "\" (травоятное)";
    }

}
