package animals;

import model.Size;

public class Penguin extends Herbivore implements Run, Swim, Voice {
    public Penguin(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.MEDIUM;
    }

    @Override
    public void swim() {
        System.out.println("Я плыву, гребя лапками с перепонками и плавниками!");
    }

    @Override
    public void run() {
        System.out.println("Я бегу в перевалочку!");
    }

    @Override
    public String getVoice() {
        return "Пи-пи-пи";
    }
}
