package animals;

import model.Size;

public class Duck extends Herbivore implements Swim, Voice, Run, Fly {

    public Duck(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }

    @Override
    public void run() {
        System.out.println("Я бегу в перевалочку!");
    }

    @Override
    public void swim() {
        System.out.println("Я плыву, гребя лапками с перепонками!");
    }

    @Override
    public String getVoice() {
        return "Кря-кря";
    }

    @Override
    public void fly() {
        System.out.println("Я лечу громко махая крыльями!");

    }
}
