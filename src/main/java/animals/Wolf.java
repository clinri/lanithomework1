package animals;

import model.Size;

public class Wolf extends Carnivorous implements Run, Swim, Voice {
    public Wolf(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.MEDIUM;
    }

    @Override
    public void run() {
        System.out.println("Я бегу безшумно!");
    }

    @Override
    public void swim() {
        System.out.println("Я плыву перебирая лапками");
    }

    @Override
    public String getVoice() {
        return "У-у-у";
    }
}
