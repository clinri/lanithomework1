package animals;

import model.Size;

public class Dolphin extends Carnivorous implements Swim, Voice {

    public Dolphin(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.LARGE;
    }

    @Override
    public void swim() {
        System.out.println("Я плыву, мощьно изгибая хвост!");

    }

    @Override
    public String getVoice() {
        return "Тра-та-та-та";
    }
}
