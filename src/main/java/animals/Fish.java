package animals;

import model.Size;

public class Fish extends Herbivore implements Swim {
    public Fish(String name) {
        super(name);
    }

    @Override
    public Size getSize() {
        return Size.MEDIUM;
    }

    @Override
    public void swim() {
        System.out.println("Я плыву чтобы дышать!");
    }
}
