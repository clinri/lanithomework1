package animals;

import model.Size;

public class Kotik extends Carnivorous implements Voice, Run {
    private static int count = 0;
    private static final int METHODS = 5;
    private String voice;
    private int satiety;
    private int weight;

    public Kotik(String name) {
        super(name);
        count++;
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }

    public Kotik(String name, String voice, int satiety, int weight) {
        super(name);
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        count++;
    }

    public static int getCount() {
        return count;
    }

    public int getSatiety() {
        return satiety;
    }

    public int getWeight() {
        return weight;
    }

    public static void setCount(int count) {
        Kotik.count = count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    boolean play() {
        if (satiety > 0) {
            System.out.println("I'm playing!");
            satiety--;
            return true;
        } else {
            System.out.println("I want to eat!");
            return false;
        }
    }

    boolean sleep() {
        if (satiety > 0) {
            System.out.println("I'm sleeping...");
            satiety--;
            return true;
        } else {
            System.out.println("I want to eat!");
            return false;
        }
    }

    boolean wash() {
        if (satiety > 0) {
            System.out.println("I'm washing!");
            satiety--;
            return true;
        } else {
            System.out.println("I want to eat!");
            return false;
        }
    }

    boolean walk() {
        if (satiety > 0) {
            System.out.println("I'm walking!");
            satiety--;
            return true;
        } else {
            System.out.println("I want to eat!");
            return false;
        }
    }

    boolean hunt() {
        if (satiety > 0) {
            System.out.println("I'm hunt!");
            satiety--;
            return true;
        } else {
            System.out.println("I want to eat!");
            return false;
        }
    }

    void eat(int satiety) {
        this.satiety += satiety;
    }

    void eat(int satiety, String food) {
        System.out.println("I'm eating " + food);
        this.satiety += satiety;
    }

    void eat() {
        eat(5, "Meat");
    }

    public String[] liveAnotherDay() {
        String[] dayOfCat = new String[24];
        for (int i = 0; i < 24; i++) {
            int toDo = (int) (Math.random() * METHODS) + 1;
            System.out.println("Method " + toDo);
            boolean satiety = true;
            switch (toDo) {
                case 1:
                    if (play())
                        dayOfCat[i] = i + " - играл";
                    else
                        satiety = false;
                    break;
                case 2:
                    if (sleep())
                        dayOfCat[i] = i + " - спал";
                    else
                        satiety = false;
                    break;
                case 3:
                    if (wash())
                        dayOfCat[i] = i + " - умывался";
                    else
                        satiety = false;
                    break;
                case 4:
                    if (walk())
                        dayOfCat[i] = i + " - гулял";
                    else
                        satiety = false;
                    break;
                case 5:
                    if (hunt())
                        dayOfCat[i] = i + " - охотился";
                    else
                        satiety = false;
                    break;
            }
            if (!satiety) {
                eat();
                dayOfCat[i] = i + " - ел";
            }
        }
        return dayOfCat;
    }

    @Override
    public void run() {
        System.out.println("Я бегу а подушечках лапок!");
    }

    @Override
    public String getVoice() {
        return voice;
    }

    @Override
    public String toString() {
        return super.toString() + " по имени " + getName();
    }
}
