package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;
import model.Size;

public abstract class Animal {
    protected String name;
    protected int satiety; //сытость

    public Animal() {
        satiety = 10;
    }

    public Animal(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract Size getSize();

    public int getSatiety() {
        return satiety;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    @Override
    public String toString() {
        String kindOfAnimal = getClass().getName();
        switch (kindOfAnimal.substring(8, kindOfAnimal.length())) {
            case "Bat":
                return "Летучая мышь";
            case "Dolphin":
                return "Дельфин";
            case "Duck":
                return "Утка";
            case "Fish":
                return "Рыба";
            case "Penguin":
                return "Пингвин";
            case "Wolf":
                return "Волк";
            case "Kotik":
                return "Котик";
            default:
                return "Неизвестное животное";
        }
    }
}
