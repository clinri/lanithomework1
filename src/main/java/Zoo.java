import animals.*;
import employee.Worker;
import food.Grass;
import food.Meat;
import food.WrongFoodException;
import model.Aviary;
import model.Size;

public class Zoo {
    static private Aviary<Carnivorous> carnivorousAviary = new Aviary<>(Size.MEDIUM);
    static private Aviary<Herbivore> herbivoreAviary = new Aviary<>(Size.SMALL);

    public static void main(String[] args) {
        Bat bat = new Bat("RedBat"); //хищьник 1
        Dolphin dolphin = new Dolphin("SmartDolphin"); //хищьник 2
        Duck duck = new Duck("SuperDuck"); //Травоядное 1
        Fish fish = new Fish("GoldFish"); //Травоядное 2
        Kotik kotik = new Kotik("Васька", "Мяяяяяяяяяяяу", 20, 3000); //хищьник 3
        Penguin penguin = new Penguin("ImperialPenguin"); //хищьник 4
        Wolf wolf = new Wolf("GreyWolf"); //хищьник 5

        Grass carrot = new Grass(20);
        Meat sausage = new Meat(15);

        Worker worker = new Worker();

        worker.feed(bat, carrot);
        worker.feed(bat, sausage);
        worker.feed(wolf, carrot);
        worker.getVoice(wolf);
        worker.getVoice(kotik);

        for (Swim schwimmends : createPond()) {
            schwimmends.swim();
        }

        fillCarnivorousAviary();
        fillHerbivoreAviary();

        System.out.println(getCarnivorous("OldWolf"));
        System.out.println(getHerbivore("CleverDuck"));
        System.out.println(herbivoreAviary.removeAnimal("AngryDuck")); // удалена на плохое поведение
    }

    static Swim[] createPond() {
        Swim[] schwimmends = new Swim[4];
        schwimmends[0] = new Dolphin("SmartDolphin");
        schwimmends[1] = new Duck("SuperDuck");
        schwimmends[2] = new Fish("GoldFish");
        schwimmends[3] = new Penguin("ImperialPenguin");
        return schwimmends;
    }

    static void fillCarnivorousAviary() {
        Wolf wolf1 = new Wolf("GreyWolf"); //хищьник 1
        Wolf wolf2 = new Wolf("WhiteWolf"); //хищьник 2
        Wolf wolf3 = new Wolf("OldWolf"); //хищьник 3
        Duck duck = new Duck("CrazyDuck");
        carnivorousAviary.addAnimal(wolf1);
        carnivorousAviary.addAnimal(wolf2);
        carnivorousAviary.addAnimal(wolf3);
        //carnivorousAviary.addAnimal(duck); // утку к волкам не пускает
    }

    static void fillHerbivoreAviary() {
        Duck duck1 = new Duck("SuperDuck"); //Травоядное 1
        Duck duck2 = new Duck("CleverDuck"); //Травоядное 2
        Duck duck3 = new Duck("AngryDuck"); //Травоядное 3
        Wolf wolf = new Wolf("CrazyWolf");
        herbivoreAviary.addAnimal(duck1);
        herbivoreAviary.addAnimal(duck2);
        herbivoreAviary.addAnimal(duck3);
        //herbivoreAviary.addAnimal(wolf); // волка в волльер не пустило
    }

    static Carnivorous getCarnivorous(String name) {
        return carnivorousAviary.getAnimal(name);
    }

    static Herbivore getHerbivore(String name) {
        return herbivoreAviary.getAnimal(name);
    }

}
