package food;

public class Grass extends Food {
    public Grass(int energy) {
        super(energy);
    }

    public Grass() {
        super();
        energy = 10;
    }

    @Override
    public int getEnergy() {
        return energy;
    }

    @Override
    public String toString() {
        return "Трава (энерегитческая ценность " + energy + ")";
    }
}
