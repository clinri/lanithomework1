package food;

public abstract class Food {
    protected int energy;

    public Food(int energy) {
        this.energy = energy;
    }

    public Food() {
    }

    public abstract int getEnergy();
}
