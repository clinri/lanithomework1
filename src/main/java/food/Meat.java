package food;

public class Meat extends Food {
    public Meat(int energy) {
        super(energy);
    }

    public Meat() {
        super();
        energy = 15;
    }

    @Override
    public int getEnergy() {
        return energy;
    }

    @Override
    public String toString() {
        return "Мясо (энерегитческая ценность " + energy + ")";
    }
}
